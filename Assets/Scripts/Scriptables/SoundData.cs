﻿using UnityEngine;

[CreateAssetMenu(fileName = "SoundsData", menuName = "Sounds")]
public class SoundData : ScriptableObject
{
    private static SoundData instance;

    public static SoundData Instance
    {
        get
        {
            if (instance == null)
                instance = Resources.Load<SoundData>("GameSounds");
            return instance;
        }
    }

    public GameSounds gameSounds;
}

[System.Serializable]
public class GameSounds
{

    [Header("The Box Click Sound")]
    public AudioClip boxClickShot;
    [Header("BGM Loop Sound")]
    public AudioClip backgroundSoundClip;

}


