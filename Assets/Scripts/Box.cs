﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{

    [Header("Board Variables")]
    public int column;
    public int row;
    public int targetX;
    public int targetY;
    public bool isMatched = false;

    #region private
    private MatchManager matchManager;
    private BoardManager board;
    private Vector2 tempPosition;
    #endregion

    // Use this for initialization
    void Start()
    {
        board = FindObjectOfType<BoardManager>();
        matchManager = FindObjectOfType<MatchManager>();
    }


    // Update is called once per frame
    void Update()
    {
        targetX = column;
        targetY = row;

        if (Mathf.Abs(targetX - transform.position.x) > .1)
        {
            //Move Towards the target
            tempPosition = new Vector2(targetX, transform.position.y);
            transform.position = Vector2.Lerp(transform.position, tempPosition, .6f);
            if (board.allBoxes[column, row] != this.gameObject)
            {
                board.allBoxes[column, row] = this.gameObject;
            }

        }
        else
        {
            //Directly set the position
            tempPosition = new Vector2(targetX, transform.position.y);
            transform.position = tempPosition;

        }


        if (Mathf.Abs(targetY - transform.position.y) > .1)
        {
            //Move Towards the target
            tempPosition = new Vector2(transform.position.x, targetY);
            transform.position = Vector2.Lerp(transform.position, tempPosition, .6f);
            if (board.allBoxes[column, row] != this.gameObject)
            {
                board.allBoxes[column, row] = this.gameObject;
            }

        }
        else
        {
            //Directly set the position
            tempPosition = new Vector2(transform.position.x, targetY);
            transform.position = tempPosition;

        }
    }

    /// <summary>
    /// Event used while player clicks on any box
    /// </summary>

    void OnMouseDown()
    {
        matchManager.AddFirstBox(column, row);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Contains("End") && !UIManager.Instance.isGameOver)
        {
            UIManager.Instance.DispatchGameOver();
        }
    }

}
