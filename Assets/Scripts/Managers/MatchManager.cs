﻿using System.Collections.Generic;
using UnityEngine;

public class MatchManager : MonoBehaviour
{

    private BoardManager board;
    public List<Box> currentMatches = new List<Box>();

    // Use this for initialization
    void Start()
    {
        board = FindObjectOfType<BoardManager>();
    }

    private void AddNewItem(Box box)
    {
        if (!currentMatches.Contains(box))
        {
            currentMatches.Add(box);
        }
        box.isMatched = true;
    }

    private void FindAllMatches()
    {
        int i = 0, j = 0;

        for (int index = 0; index < currentMatches.Count; index++)
        {

            Box currentBox = currentMatches[index];

            i = currentBox.column;

            j = currentBox.row;

            if (currentBox != null)
            {
                //===========Code to check if any box exist at left or right=======//
                if (i > 0 && i < board.width - 1)//we can remove this condition 
                {
                    if (board.allBoxes[i - 1, j] != null)//Checking for the Left box
                    {
                        CheckMatch(board.allBoxes[i - 1, j].GetComponent<Box>(), currentBox);
                    }
                    if (board.allBoxes[i + 1, j] != null)//checking for the right box
                    {
                        CheckMatch(board.allBoxes[i + 1, j].GetComponent<Box>(), currentBox);
                    }

                }
                //===========Code to check if any box exist at top or bottom=======//
                if (j > 0 && j < board.height - 1)
                {
                    if (board.allBoxes[i, j + 1] != null)//checking for the top box
                    {
                        CheckMatch(board.allBoxes[i, j + 1].GetComponent<Box>(), currentBox);
                    }
                    if (board.allBoxes[i, j - 1] != null)//checking for the bottom box
                    {
                        CheckMatch(board.allBoxes[i, j - 1].GetComponent<Box>(), currentBox);

                    }

                }
            }
        }

        calculateScore();
        board.DestroyMatches();

    }

    private void calculateScore()
    {

        int val = currentMatches.Count;
        if (val > 1)//checking if any we have more than 1 type of boxes(of same type ) in a list
        {
            UIManager.Instance.UpdateGameScore(val);
            SoundManager.Instance.PlayClickSound();
        }
     
      
    }

    private void CheckMatch(Box adjacentBox, Box currentBox)
    {
        if (adjacentBox.tag == currentBox.tag)
        {
            AddNewItem(currentBox);
            AddNewItem(adjacentBox);
        }
    }


    //===================Public methods=================//
    public void AddFirstBox(int i, int j)//being called when player clicks any box
    {
        Box selectedBox = board.allBoxes[i, j].gameObject.GetComponent<Box>();
        currentMatches.Add(selectedBox);
        FindAllMatches();
    }

}
