﻿using UnityEngine;
using System;

public class SoundManager : MonoBehaviour
{
    // Start is called before the first frame update
    #region public vars
    public static SoundManager Instance { get; private set; }
 
    #endregion

    #region private ref
    [SerializeField]
    private AudioSource BGMSource;
    [SerializeField]
    private AudioSource SingleClipSource;

    #endregion

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        SetRef();
    }

    private void SetRef()
    {
        BGMSource.clip = SoundData.Instance.gameSounds.backgroundSoundClip;
        SingleClipSource.clip = SoundData.Instance.gameSounds.boxClickShot;
        PlayBackgroundSound();
    }

    public void PlayClickSound()
    {
        SingleClipSource.Play();
    }
    public void PlayBackgroundSound()
    {
        BGMSource.Play();
        BGMSource.loop = true;
    }
}
