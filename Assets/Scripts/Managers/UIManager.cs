﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    // Start is called before the first frame update
    #region public vars
    public static UIManager Instance { get; private set; }

    public delegate void GameOver();
    public GameOver DispatchGameOver = delegate { };

    public delegate void GamePaused();
    public GameOver DispatchGamePaused = delegate { };

    public delegate void GameResumed();
    public GameOver DispatchGameResumed = delegate { };

    public bool isGameOver { get; private set; }

    #endregion

    #region private ref
    [SerializeField]
    private Text score;

    [SerializeField]
    private int scoreMultiplierFactor=100;

    [SerializeField]
    private Transform gameOverPanel;

    private int scoreVal=0;

   

    #endregion

    private void OnEnable()
    {
        DispatchGameOver += GameOverListener;
        DispatchGamePaused += GamePausedListener;
        DispatchGameResumed += GameResumedListner;
    }

    private void OnDisable()
    {
        DispatchGameOver -= GameOverListener;
        DispatchGamePaused -= GamePausedListener;
        DispatchGameResumed -= GameResumedListner;
    }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
       
    }

    void GameOverListener()
    {
        isGameOver = true;
        gameOverPanel.gameObject.SetActive(true);
    }

    void GamePausedListener()
    {

    }

    void GameResumedListner()
    {

    }

    public void UpdateGameScore(int val)
    {
        scoreVal += (val*scoreMultiplierFactor);
        score.text = scoreVal + "";
    }

    public void RestartGame()
    {
        isGameOver = false;
        gameOverPanel.gameObject.SetActive(false);
        scoreVal = 0;
        score.text = "";
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
