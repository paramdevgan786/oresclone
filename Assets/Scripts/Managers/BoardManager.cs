﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    wait,
    move
}


public class BoardManager : MonoBehaviour
{
    #region public vars
    public GameState currentState = GameState.move;
    public int width;
    public int height;
    public int offSet;
    public GameObject[] boxes;//random boxes
    public GameObject[,] allBoxes;//ref of all grid elements
    #endregion

    #region private 
    private MatchManager matchManager;
    #endregion

    // Use this for initialization
    private void Start()
    {
        matchManager = FindObjectOfType<MatchManager>();
        allBoxes = new GameObject[width, height];
        GridSetUp();
    }

    private void GridSetUp()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Vector2 tempPosition = new Vector2(i, j + offSet);

                int randomBoxIndex = Random.Range(0, boxes.Length);

                GameObject box = Instantiate(boxes[randomBoxIndex], tempPosition, Quaternion.identity);
                box.GetComponent<Box>().row = j;
                box.GetComponent<Box>().column = i;
                box.transform.parent = this.transform;
                box.name = "( " + i + ", " + j + " )";
                allBoxes[i, j] = box;
            }

        }

    }

    private void DestroyMatchesAt(int column, int row)
    {
        if (allBoxes[column, row].GetComponent<Box>().isMatched)
        {
            Destroy(allBoxes[column, row]);
            allBoxes[column, row] = null;
        }
    }

    public void DestroyMatches()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (allBoxes[i, j] != null)
                {
                    DestroyMatchesAt(i, j);
                }
            }
        }
        matchManager.currentMatches.Clear();
        StartCoroutine(DecreaseRowRoutine());
    }

    private IEnumerator DecreaseRowRoutine()
    {
        int nullCount = 0;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (allBoxes[i, j] == null)
                {
                    nullCount += 1;
                }
                else if (nullCount > 0)
                {
                    allBoxes[i, j].GetComponent<Box>().row -= nullCount;
                    allBoxes[i, j] = null;
                }
            }
            nullCount = 0;
        }
        yield return new WaitForSeconds(.4f);
        StartCoroutine(FillBoardRoutine());
    }

    private bool MatchesOnBoard()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (allBoxes[i, j] != null)
                {
                    if (allBoxes[i, j].GetComponent<Box>().isMatched)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private IEnumerator FillBoardRoutine()
    {
       
        while (MatchesOnBoard())
        {
            yield return new WaitForSeconds(.5f);
            DestroyMatches();
        }
        matchManager.currentMatches.Clear();
        yield return new WaitForSeconds(.5f);
        currentState = GameState.move;
     
    }



}
