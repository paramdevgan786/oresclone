﻿using System.Collections;
using UnityEngine;


public class GameSpeedController : MonoBehaviour
{
    #region public
    [Header("Duration to extend Grid size towards end")]
    public float duration;
    #endregion

    #region private ref
    private float startPosX;
    private float destinationPosX;
    private bool isGameOver;
    #endregion

    private void Start()
    {
        startPosX = gameObject.transform.position.x;
        StartCoroutine(IncreaseGridSizeRoutine());
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        UIManager.Instance.DispatchGameOver += GameOverListener;
        UIManager.Instance.DispatchGamePaused += GamePausedListener;
        UIManager.Instance.DispatchGameResumed += GameResumedListner;
    }

    private void OnDisable()
    {
        UIManager.Instance.DispatchGameOver -= GameOverListener;
        UIManager.Instance.DispatchGamePaused -= GamePausedListener;
        UIManager.Instance.DispatchGameResumed -= GameResumedListner;
    }

    private void GameOverListener()
    {
        isGameOver = true;
        StopCoroutine("IncreaseGridSizeRoutine");
    }

    private void GamePausedListener()
    {
        //do whatever you want
    }

    private void GameResumedListner()
    {
        //do whatever you want on resume
    }

    private void TweenCamera()
    {
        destinationPosX = startPosX + 1;
        LeanTween.moveX(gameObject, destinationPosX, .5f);
        startPosX = destinationPosX;
    }

    private IEnumerator IncreaseGridSizeRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(duration);

            if(!isGameOver)
            TweenCamera();
        }
       
    }
}
